﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyViolet : MonoBehaviour
{
    public GameObject gameManager, explosion, soundManager;
    int cont = 0;
    float x = 2f, y = 2f, z = 2f;
    private void OnTriggerEnter(Collider colObj)
    {
        if (colObj.gameObject.tag == "Bullet")
        {
            cont++;
            transform.localScale = new Vector3(x=x-0.5f , y=y-0.5f , z=z-0.5f );
            soundManager.SendMessage("PlayExplosionSound");
            Destroy(colObj.gameObject);
        }
        if (cont == 3) 
        { 
            soundManager.SendMessage("PlayExplosionSound");
            gameManager.SendMessage("EnemyDownCounter");
            GameObject newExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(newExplosion, 2);
            Destroy(gameObject);
            Destroy(colObj.gameObject);
        }
        if (colObj.gameObject.name == "Player")
        {
            Destroy(colObj.gameObject);
        }

    }
}
