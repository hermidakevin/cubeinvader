﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float fireRate = 1f;
    float nextFire = 0f;

    public GameObject bullet, soundManager;
    private CharacterController playerController;
    private Vector3 movement = Vector3.zero;

    void Start()
    {
        playerController = GetComponent<CharacterController>();
    }
    void Update()
    {
        movement = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
        movement = transform.TransformDirection(movement);
        playerController.Move(movement * Time.deltaTime * 50);

        if(Input.GetKeyDown("space") && Time.time>nextFire)
        {
            soundManager.SendMessage("PlayBulletSound");
            nextFire = Time.time + fireRate;

            GameObject newBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            newBullet.GetComponent<Rigidbody>().AddForce(Vector3.up * 2000);
            Destroy(newBullet, 3);
        }
    }

    }
