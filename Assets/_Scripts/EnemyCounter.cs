﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyCounter : MonoBehaviour
{
    private int score = 0;
    public Text scoretext;
    public Image levelimage;

    public int enemyCount;
    public void EnemyDownCounter ()
    {
        enemyCount++;

        if (enemyCount == 12)
        {
            SceneManager.LoadScene("Win");
        }
    }
    private void Update()
    {
        scoretext.text = "Score: " + enemyCount;
        
    }


}
